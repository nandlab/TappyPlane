using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

namespace TappyPlane {
    public class Plane : MonoBehaviour
    {
        [SerializeField] GameObject scoreText;
        [SerializeField] GameObject powerupText;
        TextMeshProUGUI scoreTextMesh;
        TextMeshProUGUI powerupTextMesh;
        AudioSource[] soundEffects;
        bool jump = false;
        bool crashed = false;
        int score = 0;
        float scoreBoostUntil = 0.0f;

        float tryAgainTime;

        enum SFX {
            HIT,
            JUMP,
            POINT,
            TOTAL
        };

        // Start is called before the first frame update
        void Start()
        {
            scoreTextMesh = scoreText.GetComponent<TextMeshProUGUI>();
            powerupTextMesh = powerupText.GetComponent<TextMeshProUGUI>();
            soundEffects = GetComponents<AudioSource>();
            Debug.Assert(soundEffects.Length >= ((int)SFX.TOTAL));
        }

        void FixedUpdate()
        {
            if (jump && !crashed)
            {
                soundEffects[((int)SFX.JUMP)].Play(); 
                Rigidbody2D rb = GetComponent<Rigidbody2D>();
                rb.velocity += new Vector2(0, 5);
                jump = false;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                jump = true;
            }
            if (crashed && Time.time > tryAgainTime)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            float powerupTimeLeft = scoreBoostUntil - Time.time;
            if (powerupTimeLeft > 0.0f)
            {
                powerupTextMesh.text = $"Boost: {powerupTimeLeft:0.0}";
            }
            else
            {
                powerupTextMesh.text = "";
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.name == "Ceiling")
                return;

            crashed = true;
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            sr.color = new Color(0.7f, 0.5f, 0.5f);
            tryAgainTime = Time.time + 2.5f;

            soundEffects[((int)SFX.HIT)].Play(); 
        }

        void OnTriggerEnter2D(Collider2D trigger)
        {
            if (!crashed)
            {
                bool updateScore = false;
                switch(trigger.gameObject.tag)
                {
                    case "Obstacle":
                        score++;
                        updateScore = true;
                        break;
                    case "SpecialObstacle":
                        if (Time.time < scoreBoostUntil)
                            score += 3;
                        else
                            score++;
                        updateScore = true;
                        break;
                    case "Powerup":
                        scoreBoostUntil = Time.time + 15.0f;
                        Destroy(trigger.gameObject);
                        break;
                    default:
                        break;
                }
                if (updateScore)
                {
                    soundEffects[((int)SFX.POINT)].Play(); 
                    scoreTextMesh.text = $"Score: {score}";
                }
            }
        }
    }
}
