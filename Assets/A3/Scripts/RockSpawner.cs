using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TappyPlane {
    public class RockSpawner : MonoBehaviour
    {
        public GameObject[] prefabRockGrounds;
        public GameObject[] prefabRockCeilings;
        public GameObject specialObstacle;
        public GameObject prefabPowerup;
        float nextSpawn;
        bool nextRockOrientation = false;
        int biome = 0;
        int biomeCountdown;
        int biomes;
        int countdown = 6;
        int powerupCountdown;

        private void instantiateRock()
        {
            if (countdown <= 0)
            {
                GameObject obstacle = Instantiate(specialObstacle, transform);
                obstacle.transform.localPosition = new Vector3(10, 0, 0);
                countdown = 6;
            }
            else
            {
                if (powerupCountdown <= 0)
                {
                    GameObject powerup = Instantiate(prefabPowerup, transform);
                    float y = nextRockOrientation ? 3.5f : -3.5f;
                    powerup.transform.localPosition = new Vector3(10, y, 0);
                    powerupCountdown = Random.Range(10, 15);
                }
                else
                {
                    powerupCountdown--;
                }

                if (biomeCountdown <= 0)
                {
                    int newBiome = Random.Range(0, biomes - 1);
                    if (newBiome >= biome)
                    {
                        newBiome++;
                    }
                    biome = newBiome;
                    biomeCountdown = Random.Range(4, 10);
                }
                GameObject prefab = nextRockOrientation ? prefabRockCeilings[biome] : prefabRockGrounds[biome];
                nextRockOrientation = !nextRockOrientation;
                GameObject rock = Instantiate(prefab, transform);
                SpriteRenderer rockSR = rock.GetComponent<SpriteRenderer>();
                Transform rockTransform = rock.GetComponent<Transform>();
                Vector3 rockPosition = rockTransform.localPosition;
                Vector3 rockScale = rockTransform.localScale;
                // Debug.Log(rockSR.localBounds);

                Vector3 rockBoundsMax = rockSR.localBounds.max;
                float randomScale = Random.Range(2.0f, 2.6f);
                if (nextRockOrientation)
                {
                    randomScale *= -1;
                    rockScale.y = randomScale;
                    rockPosition.y = rockScale.y * rockBoundsMax.y + 5.5f;
                }
                else
                {
                    rockScale.y = randomScale;
                    rockPosition.y = rockScale.y * rockBoundsMax.y - 5.0f;
                }
                rockScale.x = Random.Range(2.0f, 4.0f);
                rockTransform.localPosition = rockPosition;
                rockTransform.localScale = rockScale;
                countdown--;
            }
            nextSpawn = Time.time + Random.Range(1.7f, 2.3f);
            biomeCountdown--;
        }

        // Start is called before the first frame update
        void Start()
        {
            powerupCountdown = Random.Range(10, 15);
            Debug.Assert(prefabRockGrounds.Length == prefabRockCeilings.Length);
            biomes = prefabRockGrounds.Length;
            Debug.Assert(biomes > 1);
            biomeCountdown = Random.Range(4, 10);
            instantiateRock();
        }

        // Update is called once per frame
        void Update()
        {
            if (Time.time > nextSpawn)
            {
                instantiateRock();
            }
        }
    }
}
